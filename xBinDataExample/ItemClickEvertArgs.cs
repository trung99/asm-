﻿using xBinDataExample.Models;

namespace xBinDataExample
{
    internal class ItemClickEvertArgs
    {
        public Book ClickedItem { get; internal set; }
    }
}